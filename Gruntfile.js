module.exports = function (grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    // kompilace a minifikace LESS souboru
    less: {
      dev: {
        options: {
          compress: false
        },
        files: {
          "style.css": ["style/all.less" ]
        }
      }
    },

    // kompilace pug
    pug: {
      compile: {
        options: {
          pretty: true,
        },
        files:[
          {
            expand: true,
            cwd: '',
            ext: '.html',
            extDot: 'last',
            src: [
              '*.pug'
            ],
            dest: ''
          }
        ]
      }
    },

    // sledovani zmen ve vsech LESS souborech
    watch: {
      less:{
        files: ['style/*.less','style/**/*.less'],
        tasks: ['less']
      },
      pug:{
        files: ['*.pug', 'includes/*.pug'],
        tasks: ['pug']
      }
    }
});

// These plugins provide necessary tasks.
grunt.loadNpmTasks('grunt-contrib-pug');
// Default task.
grunt.registerTask('build', 'Convert pug templates into html templates', ['less', 'pug']);

grunt.loadNpmTasks('grunt-contrib-watch');
grunt.loadNpmTasks('grunt-contrib-less');

grunt.registerTask('default', ['less']);
};
